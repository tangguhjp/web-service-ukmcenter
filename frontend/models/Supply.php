<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "supply".
 *
 * @property integer $ukm_id
 * @property integer $product_id
 * @property string $upload_time
 *
 * @property Ukm $ukm
 * @property Product $product
 */
class Supply extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'supply';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ukm_id', 'product_id'], 'required'],
            [['ukm_id', 'product_id'], 'integer'],
            [['upload_time'], 'safe'],
            [['ukm_id'], 'exist', 'skipOnError' => true, 'targetClass' => Ukm::className(), 'targetAttribute' => ['ukm_id' => 'ukm_id']],
            [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Product::className(), 'targetAttribute' => ['product_id' => 'product_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ukm_id' => 'Ukm ID',
            'product_id' => 'Product ID',
            'upload_time' => 'Upload Time',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUkm()
    {
        return $this->hasOne(Ukm::className(), ['ukm_id' => 'ukm_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['product_id' => 'product_id']);
    }
}
