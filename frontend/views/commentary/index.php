<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\CommentarySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Commentaries';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="commentary-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Commentary', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'user_id',
            'product',
            'comment:ntext',
            'comment_time',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
