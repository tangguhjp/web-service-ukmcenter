<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\DirMessage */

$this->title = 'Create Dir Message';
$this->params['breadcrumbs'][] = ['label' => 'Dir Messages', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="dir-message-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
