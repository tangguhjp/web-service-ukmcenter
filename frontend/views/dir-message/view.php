<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\models\DirMessage */

$this->title = $model->sender_id;
$this->params['breadcrumbs'][] = ['label' => 'Dir Messages', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="dir-message-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'sender_id' => $model->sender_id, 'receiver_id' => $model->receiver_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'sender_id' => $model->sender_id, 'receiver_id' => $model->receiver_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'sender_id',
            'receiver_id',
            'message:ntext',
            'message_time',
        ],
    ]) ?>

</div>
