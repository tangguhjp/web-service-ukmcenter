<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\DirMessage */

$this->title = 'Update Dir Message: ' . $model->sender_id;
$this->params['breadcrumbs'][] = ['label' => 'Dir Messages', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->sender_id, 'url' => ['view', 'sender_id' => $model->sender_id, 'receiver_id' => $model->receiver_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="dir-message-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
