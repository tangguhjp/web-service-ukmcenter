<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\DirMessageSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Dir Messages';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="dir-message-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Dir Message', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'sender_id',
            'receiver_id',
            'message:ntext',
            'message_time',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
