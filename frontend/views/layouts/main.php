<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use common\widgets\Alert;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => 'UKM-CENTER',
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);
    $menuItems = [
        ['label' => 'Home', 'url' => ['/site/index']],
    ];
    if (Yii::$app->user->isGuest) {
        $menuItems[] = ['label' => 'Signup', 'url' => ['/site/signup']];
        $menuItems[] = ['label' => 'Login', 'url' => ['/site/login']];
    } else {
        $menuItems[] = '<li>'
            . Html::beginForm(['/user'], 'get')
            . Html::submitButton('Users',
                ['class' => 'btn btn-link logout']
                )
            . Html::endForm()
            . '</li>'
            . '<li>'
            . Html::beginForm(['/ukm'], 'get')
            . Html::submitButton('Ukm',
                ['class' => 'btn btn-link logout']
                )
            . Html::endForm()
            . '</li>'
            . '<li>'
            . Html::beginForm(['/product'], 'get')
            . Html::submitButton('Product',
                ['class' => 'btn btn-link logout']
                )
            . Html::endForm()
            . '</li>'
            . '<li>'
            . Html::beginForm(['/supply'], 'get')
            . Html::submitButton('Suppliers',
                ['class' => 'btn btn-link logout']
                )
            . Html::endForm()
            . '</li>'
            . '<li>'
            . Html::beginForm(['/category'], 'get')
            . Html::submitButton('Category',
                ['class' => 'btn btn-link logout']
                )
            . Html::endForm()
            . '</li>'
            . '<li>'
            . Html::beginForm(['/commentary'], 'get')
            . Html::submitButton('Commentary',
                ['class' => 'btn btn-link logout']
                )
            . Html::endForm()
            . '</li>'
            . '<li>'
            . Html::beginForm(['/dir-message'], 'get')
            . Html::submitButton('Direct Messages',
                ['class' => 'btn btn-link logout']
                )
            . Html::endForm()
            . '</li>'
            . '<li>'
            . Html::beginForm(['/site/logout'], 'post')
            . Html::submitButton(
                'Logout (' . Yii::$app->user->identity->username . ')',
                ['class' => 'btn btn-link logout']
            )
            . Html::endForm()
            . '</li>'
            ;
    }
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => $menuItems,
    ]);
    NavBar::end();
    ?>

    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; UKM-Center <?= date('Y') ?></p>

        <p class="pull-right"><?= Yii::powered() ?></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
