<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\Ukm */

$this->title = 'Update Ukm: ' . $model->ukm_id;
$this->params['breadcrumbs'][] = ['label' => 'Ukms', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->ukm_id, 'url' => ['view', 'id' => $model->ukm_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="ukm-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
