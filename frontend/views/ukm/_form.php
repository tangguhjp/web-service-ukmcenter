<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\Ukm */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="ukm-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'ukm_owner')->textInput() ?>

    <?= $form->field($model, 'ukm_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'ukm_address')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'ukm_telp')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'ukm_description')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'ukm_fp')->fileInput(['maxlength'=> true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
