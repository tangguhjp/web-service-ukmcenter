<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\UkmSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Ukms';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ukm-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Ukm', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'ukm_id',
            'ukm_owner',
            'ukm_name',
            'ukm_address',
            'ukm_telp',
            // 'ukm_description:ntext',
            // 'ukm_fp',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
