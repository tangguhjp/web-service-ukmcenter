<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Ukm */

$this->title = 'Create Ukm';
$this->params['breadcrumbs'][] = ['label' => 'Ukms', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ukm-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
