<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\UkmSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="ukm-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'ukm_id') ?>

    <?= $form->field($model, 'ukm_owner') ?>

    <?= $form->field($model, 'ukm_name') ?>

    <?= $form->field($model, 'ukm_address') ?>

    <?= $form->field($model, 'ukm_telp') ?>

    <?php // echo $form->field($model, 'ukm_description') ?>

    <?php // echo $form->field($model, 'ukm_fp') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
