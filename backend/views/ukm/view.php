<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\Ukm */

$this->title = $model->ukm_id;
$this->params['breadcrumbs'][] = ['label' => 'Ukms', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ukm-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->ukm_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->ukm_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'ukm_id',
            'ukm_owner',
            'ukm_name',
            'ukm_address',
            'ukm_telp',
            'ukm_description:ntext',
            'ukm_fp',
        ],
    ]) ?>

</div>
