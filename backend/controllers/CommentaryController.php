<?php

namespace backend\controllers;

use Yii;
use backend\models\Commentary;
use backend\models\CommentarySearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * CommentaryController implements the CRUD actions for Commentary model.
 */
class CommentaryController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Commentary models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CommentarySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Commentary model.
     * @param integer $user_id
     * @param integer $product
     * @return mixed
     */
    public function actions(){
        Yii::$app->response->format=\yii\web\Response::FORMAT_JSON;
        return[
            'error'=>[
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha'=>[
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' =>YII_ENV_TEST? 'testme':null,
            ],
        ];
    }
    public function actionView()
    {
        return $this->findDataAll();
    }

    /**
     * Creates a new Commentary model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Commentary();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'user_id' => $model->user_id, 'product' => $model->product]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Commentary model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $user_id
     * @param integer $product
     * @return mixed
     */
    public function actionUpdate($user_id, $product)
    {
        $model = $this->findModel($user_id, $product);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'user_id' => $model->user_id, 'product' => $model->product]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Commentary model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $user_id
     * @param integer $product
     * @return mixed
     */
    public function actionDelete($user_id, $product)
    {
        $this->findModel($user_id, $product)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Commentary model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $user_id
     * @param integer $product
     * @return Commentary the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($user_id, $product)
    {
        if (($model = Commentary::findOne(['user_id' => $user_id, 'product' => $product])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    protected function findDataAll(){
        if(($model= Commentary::find()->all())!==null){
            return $model;
        }else{
            throw new NotFoundHttpException('Not Found');
        }
    }
}
