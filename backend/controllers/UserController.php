<?php

namespace backend\controllers;

use Yii;
use backend\models\User;
use backend\models\UserSearch;
use common\models\LoginForm;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\rest\ActiveController;
use yii\filters\VerbFilter;
use yii\web\BadRequestHttpException;
use yii\filters\auth\CompositeAuth;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\auth\QueryParamAuth;


/**
 * UserController implements the CRUD actions for User model.
 */
class UserController extends ActiveController
{
    /**
     * @inheritdoc
     */
    public $modelClass = 'backend\models\User';

    public function behaviors()
    {
        return [
            'authenticator' => [
                'class' => CompositeAuth::className(),
                'except' => ['create'],
                'only' => ['logout','signout','view','update'],
                'authMethods' => [
                    HttpBasicAuth::className(),
                    HttpBearerAuth::className(),
                    QueryParamAuth::className(),
                ],
            ],
        ];
    }

    /**
     * Lists all User models.
     * @return mixed
     */

    public function actions(){
        Yii::$app->response->format=\yii\web\Response::FORMAT_JSON;
        return[
            'error'=>[
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha'=>[
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' =>YII_ENV_TEST? 'testme':null,
            ],
        ];
    }
    public function actionIndex()
    {
    //     $searchModel = new UserSearch();
    //     $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

    //     return $this->render('index', [
    //         'searchModel' => $searchModel,
    //         'dataProvider' => $dataProvider,
        // ]);
        return $this->findDataAll();
    }

    /**
     * Displays a single User model.
     * @param integer $id
     * @return mixed
     */
    public function actionView()
    {   
        $model = \Yii::$app->user->identity;
        if ($model === null) {
            throw new NotFoundHttpException;
        }
        return $model;
    }

    /**
     * Creates a new User model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new User();
        if ($model->load(Yii::$app->request->post())) {
            $model->password=password_hash($model->password, PASSWORD_DEFAULT);
            if($model->save()){
                return['msg'=>'success'];
            }else{
                return array('message'=>'gagal', 'data'=>$model->getErrors());
            }
            return "again!";
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }
    /**
     * login
     */
    public function actionLogin()
    {
        // check login
        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            $username = $model->username;
            $password = $model->password;
            // make auth key for android
            $authKey = hash('md5',$password . rand(1,10));
            
            // return "fgh";
            $user = $this->findModel($username);
            $user->auth_key = $authKey;
            if ($user->update(false)) {
                return ['msg'=>'success'];
            }
            return "non";
        } else {
            return ['messege' => "login error"];
        }
        return ['messege' => "please post you username and password"];
    }

    public function actionLogout()
    {
        $logout = Yii::$app->user->logout();
        if($logout){
            return ['message' => 'logout success'];
        }else{
            return ['message' => 'logout failed'];
        }
    }
    protected function findModelId($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    // public function actionOut()
    // {
    //     $user = \Yii::$app->user->identity;
    //     $id=$user['id'];
    //     $model = $this->findModelId($id);
    //     return $model;
    //     $model->auth_key = '';
    //     if ($model->update(false)) {
    //         return ['messege'=>'logout success'];
    //     }
    //     return ['messege'=>'logout failed'];
    // }
    /**
     * 
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate()
    {
        $user = \Yii::$app->user->identity;
        $id=$user['id'];
        // $model = $this->findModelId($user->id);
        // return "cok";
        // if ($model->load(Yii::$app->request->post())) {

        //     // bila user tidak mengedit password
        //     if(empty(@$model->password) || $model->password == 0 || $model->password == null){
        //         $model->password = $user->password;
        //     }else{
        //         $model->password = password_hash($model->password, PASSWORD_DEFAULT);            
        //     }
        //     // menyimpan
        //     if($model->save()){
        //         return ['message' => 'update complete'];
        //     }else{
        //         return ['message' => 'update failed'];
        //     }
        // }
        // return "back away!";
        $model = $this->findModelId($id);
        
        $attributes = \Yii::$app->request->post();
    
        if($model->load($attributes)){
            $model->password=password_hash($model->password, PASSWORD_DEFAULT);
            if($model->update(false)){
                return $this->findDataAll();                
            }
        }else{
            return array('status'=> false, 'data'=>'Update Data Failed');
        }
    }

    /**
     * Deletes an existing User model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        if($this->findModelId($id)->delete()){
           return array('status'=>true, 'message' => 'Delete User Successfully');
       }else{
           return array('status'=>false, 'message'=> 'Delete Failed');
       }
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($username)
    {
        if (($model = User::findOne(['username'=>$username])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function findDataAll(){
        if(($model= User::find()->all())!==null){
            return $model;
        }else{
            throw new NotFoundHttpException('Not Found');
        }
    }
}
