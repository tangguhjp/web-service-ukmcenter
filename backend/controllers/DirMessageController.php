<?php

namespace backend\controllers;

use Yii;
use backend\models\DirMessage;
use backend\models\DirMessageSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * DirMessageController implements the CRUD actions for DirMessage model.
 */
class DirMessageController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all DirMessage models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new DirMessageSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single DirMessage model.
     * @param integer $sender_id
     * @param integer $receiver_id
     * @return mixed
     */
    public function actionView($sender_id, $receiver_id)
    {
        return $this->render('view', [
            'model' => $this->findModel($sender_id, $receiver_id),
        ]);
    }

    /**
     * Creates a new DirMessage model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new DirMessage();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'sender_id' => $model->sender_id, 'receiver_id' => $model->receiver_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing DirMessage model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $sender_id
     * @param integer $receiver_id
     * @return mixed
     */
    public function actionUpdate($sender_id, $receiver_id)
    {
        $model = $this->findModel($sender_id, $receiver_id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'sender_id' => $model->sender_id, 'receiver_id' => $model->receiver_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing DirMessage model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $sender_id
     * @param integer $receiver_id
     * @return mixed
     */
    public function actionDelete($sender_id, $receiver_id)
    {
        $this->findModel($sender_id, $receiver_id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the DirMessage model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $sender_id
     * @param integer $receiver_id
     * @return DirMessage the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($sender_id, $receiver_id)
    {
        if (($model = DirMessage::findOne(['sender_id' => $sender_id, 'receiver_id' => $receiver_id])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
