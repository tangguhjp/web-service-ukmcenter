<?php

namespace backend\controllers;

use Yii;
use backend\models\Ukm;
use backend\models\UkmSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\rest\ActiveController;


/**
 * UkmController implements the CRUD actions for Ukm model.
 */
class UkmController extends ActiveController
{
    /**
     * @inheritdoc
     */
    public $modelClass = 'backend\models\Ukm';
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],

        ];
    }

    /**
     * Lists all Ukm models.
     * @return mixed
     */
    public function actionIndex()
    {
        // $searchModel = new Ukm();
        // $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        // return $this->render('index', [
        //     'searchModel' => $searchModel,
        //     'dataProvider' => $dataProvider,
        // ]);
        return $this->findDataAll();
    }

    /**
     * Displays a single Ukm model.
     * @param integer $id
     * @return mixed
     */
    public function actions(){
        Yii::$app->response->format=\yii\web\Response::FORMAT_JSON;
        return[
            'error'=>[
                'class'=>'yii\web\ErrorAction',
            ],
            'captcha'=>[
                'class'=>'yii\captcha\CaptchaAction',
                'fixedVerifyCode'=> YII_ENV_TEST? 'testme':null,
            ]
        ];
    }
    public function actionView($id)
    {
        return $this->findByOwner($id);
    }

    /**
     * Creates a new Ukm model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Ukm();

        if ($model->load(Yii::$app->request->post())) {
            if($model->save()){
                return ['message'=>'success'];
            }else{
                return ['message'=>'One User only allowed have one UKM'];
            }
        } else {
            return "gagal load";
        }
        // $model = new User();
        // if ($model->load(Yii::$app->request->post())) {
        //     $model->password=password_hash($model->password, PASSWORD_DEFAULT);
        //     if($model->save()){
        //         return['message'=>'berhasil'];
        //     }else{
        //         return array('message'=>'gagal', 'data'=>$model->getErrors());
        //     }
        //     return "again!";
        // } else {
        //     return $this->render('create', [
        //         'model' => $model,
        //     ]);
        // }
    }

    /**
     * Updates an existing Ukm model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate()
    {   
        $user = \Yii::$app->user->identity;
        $model = new Ukm();
        $model = $model->getUkmByIdUser($user['id']);
        if ($model->load(Yii::$app->request->post())) {
            if($model->save()){
                return $this->redirect(['view', 'id' => $model->ukm_id]);
            }else{
                return "Gagal";
            }
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Ukm model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete()
    {
        $user = \Yii::$app->user->identity;
        $model = new Ukm();
        $model = $model->deleteByIdUser($user['id']);

        return "guguk";
    }

    /**
     * Finds the Ukm model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Ukm the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Ukm::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function findByOwner($id){
        if($model = Ukm::find()->where(['ukm_owner'=>$id])->all()){
            return $model;
        }
    }
    
    protected function findDataAll(){
        if(($model= Ukm::find()->all())!==null){
            return $model;
        }else{
            throw new NotFoundHttpException('Not Found');
        }
    }
}
