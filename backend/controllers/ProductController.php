<?php

namespace backend\controllers;

use Yii;
use backend\models\Product;
use backend\models\Ukm;
use backend\models\User;
use frontend\models\ProductSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\rest\ActiveController;
use yii\filters\auth\CompositeAuth;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\auth\QueryParamAuth;


/**
 * ProductController implements the CRUD actions for Product model.
 */
class ProductController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        // return [
        //     'verbs' => [
        //         'class' => VerbFilter::className(),
        //         'actions' => [
        //             'delete' => ['POST'],
        //         ],
        //     ],
        // ];
        return [
            'authenticator' => [
                'class' => CompositeAuth::className(),
                'except' => ['create'],
                'only' => ['update','view'],
                'authMethods' => [
                    HttpBasicAuth::className(),
                    HttpBearerAuth::className(),
                    QueryParamAuth::className(),
                ],
            ],
        ];
    }

    /**
     * Lists all Product models.
     * @return mixed
     */
    public function actionIndex()
    {
        // $searchModel = new ProductSearch();
        // $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        // return $this->render('index', [
        //     'searchModel' => $searchModel,
        //     'dataProvider' => $dataProvider,
        // ]);
        return $this->findDataAll();
    }

    /**
     * Displays a single Product model.
     * @param integer $id
     * @return mixed
     */
    public function actions(){
        Yii::$app->response->format=\yii\web\Response::FORMAT_JSON;
        return[
            'error'=>[
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha'=>[
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' =>YII_ENV_TEST? 'testme':null,
            ],
        ];
    }
    public function actionView()
    {
        $user = \Yii::$app->user->identity;
        $user = User::findOne($user['id']);
        $ukm = $user->getUkms()->one();
        $ukm = Ukm::findOne($ukm['ukm_id']);
        $product = $ukm->getProducts()->all();
        return $product;
        // $product = new Product();

        
        // if(($model = Product::find()->where(['product_id'=>$id])->all())!==null){
        //     return $model;
        // }
    }

    /**
     * Creates a new Product model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Product();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->product_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Product model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->product_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Product model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Product model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Product the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUkm($id){
        if($model = Product::find()->where(['product_ukm'=>$id])->all()){
            return $model;
        }
    }
    public function actionCategory($id){
        if($model = Product::find()->where(['product_category'=>$id])->all()){
            return $model;
        }
    }
    protected function findModel($id)
    {
        if (($model = Product::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function findDataAll(){
        if(($model=Product::find()->all())!==null){
            return $model;
        }else{
            throw new NotFoundHttpException('Not Found');
        }
    }
}
