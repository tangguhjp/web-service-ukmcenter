<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "commentary".
 *
 * @property integer $user_id
 * @property integer $product
 * @property string $comment
 * @property string $comment_time
 *
 * @property User $user
 * @property Product $product0
 */
class Commentary extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'commentary';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'product', 'comment', 'comment_time'], 'required'],
            [['user_id', 'product'], 'integer'],
            [['comment'], 'string'],
            [['comment_time'], 'safe'],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
            [['product'], 'exist', 'skipOnError' => true, 'targetClass' => Product::className(), 'targetAttribute' => ['product' => 'product_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'user_id' => 'User ID',
            'product' => 'Product',
            'comment' => 'Comment',
            'comment_time' => 'Comment Time',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct0()
    {
        return $this->hasOne(Product::className(), ['product_id' => 'product']);
    }
}
