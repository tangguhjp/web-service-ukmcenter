<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "product".
 *
 * @property integer $product_id
 * @property integer $product_category
 * @property integer $product_ukm
 * @property string $product_name
 * @property string $product_description
 * @property string $product_price
 * @property resource $product_foto
 *
 * @property Commentary[] $commentaries
 * @property User[] $users
 * @property Category $productCategory
 * @property Ukm $productUkm
 */
class Product extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['product_category', 'product_ukm'], 'integer'],
            [['product_description', 'product_foto'], 'string'],
            [['product_ukm'], 'required'],
            [['product_name', 'product_price'], 'string', 'max' => 50],
            [['product_category'], 'exist', 'skipOnError' => true, 'targetClass' => Category::className(), 'targetAttribute' => ['product_category' => 'category_id']],
            [['product_ukm'], 'exist', 'skipOnError' => true, 'targetClass' => Ukm::className(), 'targetAttribute' => ['product_ukm' => 'ukm_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'product_id' => 'Product ID',
            'product_category' => 'Product Category',
            'product_ukm' => 'Product Ukm',
            'product_name' => 'Product Name',
            'product_description' => 'Product Description',
            'product_price' => 'Product Price',
            'product_foto' => 'Product Foto',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCommentaries()
    {
        return $this->hasMany(Commentary::className(), ['product' => 'product_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasMany(User::className(), ['id' => 'user_id'])->viaTable('commentary', ['product' => 'product_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductCategory()
    {
        return $this->hasOne(Category::className(), ['category_id' => 'product_category']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductUkm()
    {
        return $this->hasOne(Ukm::className(), ['ukm_id' => 'product_ukm']);
    }

    // public function getProductUser($id){
    //     return static::find()->where());
        
    // }
}
