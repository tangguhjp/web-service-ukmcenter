<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "user".
 *
 * @property integer $id
 * @property string $name
 * @property string $username
 * @property string $email
 * @property string $password
 * @property string $auth_key
 * @property integer $status
 * @property resource $user_fp
 *
 * @property Commentary[] $commentaries
 * @property Product[] $products
 * @property DirMessage[] $dirMessages
 * @property DirMessage[] $dirMessages0
 * @property User[] $receivers
 * @property User[] $senders
 * @property Ukm[] $ukms
 */
class User extends \yii\db\ActiveRecord implements \yii\web\IdentityInterface
{
    const SCENARIO_CREATE= 'create';
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fullname','email', 'password'], 'required'],
            [['password', 'user_fp'], 'string'],
            [['status'], 'integer'],
            [['fullname','username', 'email'], 'string', 'max' => 50],
        ];
    }
    public function scenarios(){
        $scenarios = parent::scenarios();
        $scenarios['create']=['fullname','username','email','password','status'];
        return $scenarios;
    }
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'fullname' => 'Fullname',
            'username' => 'Username',
            'email' => 'Email',
            'password' => 'Password',
            'auth_key' => 'Auth Key',
            'status' => 'Status',
            'user_fp' => 'User Fp',
        ];
    }
    public function getId()
    {
        return $this->id;
    }
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCommentaries()
    {
        return $this->hasMany(Commentary::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProducts()
    {
        return $this->hasMany(Product::className(), ['product_id' => 'product'])->viaTable('commentary', ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDirMessages()
    {
        return $this->hasMany(DirMessage::className(), ['sender_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDirMessages0()
    {
        return $this->hasMany(DirMessage::className(), ['receiver_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReceivers()
    {
        return $this->hasMany(User::className(), ['id' => 'receiver_id'])->viaTable('dir_message', ['sender_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSenders()
    {
        return $this->hasMany(User::className(), ['id' => 'sender_id'])->viaTable('dir_message', ['receiver_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUkms()
    {
        return $this->hasMany(Ukm::className(), ['ukm_owner' => 'id']);
    }
        public function getAuthKey()
    {
        return $this->auth_key;
    }

    public function validateAuthKey($authKey)
    {
        return $this->auth_key === $authKey;
    }
    public static function findIdentity($id)
    {
        return static::findOne($id);
    }
    public static function findIdentityByAccessToken($token, $type = null)
    {
        return static::findOne(['auth_key' => $token]);
    }

    public static function findByUsername($username) {
        return self::findOne(['username'=>$username]);
    }

    public function validatePassword($password){
        return password_verify($password,$this->password);
    }

    // public function getProduct($id){
    //     return static::find()->with
    // }
}
