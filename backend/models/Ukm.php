<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "ukm".
 *
 * @property integer $ukm_id
 * @property integer $ukm_owner
 * @property string $ukm_name
 * @property string $ukm_address
 * @property string $ukm_telp
 * @property string $ukm_description
 * @property resource $ukm_fp
 *
 * @property Product[] $products
 * @property User $ukmOwner
 */
class Ukm extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ukm';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ukm_owner', 'ukm_name', 'ukm_description'], 'required'],
            [['ukm_owner'], 'integer'],
            [['ukm_owner'], 'unique'],
            [['ukm_description', 'ukm_fp'], 'string'],
            [['ukm_name'], 'string', 'max' => 25],
            [['ukm_address'], 'string', 'max' => 100],
            [['ukm_telp'], 'string', 'max' => 15],
            [['ukm_owner'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['ukm_owner' => 'id']],
        ];
    }
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ukm_id' => 'Ukm ID',
            'ukm_owner' => 'Ukm Owner',
            'ukm_name' => 'Ukm Name',
            'ukm_address' => 'Ukm Address',
            'ukm_telp' => 'Ukm Telp',
            'ukm_description' => 'Ukm Description',
            'ukm_fp' => 'Ukm Fp',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProducts()
    {
        return $this->hasMany(Product::className(), ['product_ukm' => 'ukm_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUkmOwner()
    {
        return $this->hasOne(User::className(), ['id' => 'ukm_owner']);
    }

    public function getUkmByIdUser($id){
        return static::findOne(['ukm_owner' => $id]);
    }

    public function deleteByIdUser($id){
        static::findOne(['ukm_owner' => $id])->delete();
        return true;
    }
}
