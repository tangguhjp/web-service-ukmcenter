<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Ukm;

/**
 * UkmSearch represents the model behind the search form about `backend\models\Ukm`.
 */
class UkmSearch extends Ukm
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ukm_id', 'ukm_owner'], 'integer'],
            [['ukm_name', 'ukm_address', 'ukm_telp', 'ukm_description', 'ukm_fp'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Ukm::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'ukm_id' => $this->ukm_id,
            'ukm_owner' => $this->ukm_owner,
        ]);

        $query->andFilterWhere(['like', 'ukm_name', $this->ukm_name])
            ->andFilterWhere(['like', 'ukm_address', $this->ukm_address])
            ->andFilterWhere(['like', 'ukm_telp', $this->ukm_telp])
            ->andFilterWhere(['like', 'ukm_description', $this->ukm_description])
            ->andFilterWhere(['like', 'ukm_fp', $this->ukm_fp]);

        return $dataProvider;
    }
}
